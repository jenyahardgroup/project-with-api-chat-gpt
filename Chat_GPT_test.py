import pandas as pd
import openai
import re
import os

filename='source_db.csv'
content='' #переменная, в которую дальше сформируем и запишем запрос к CHATGPT
openai.api_key = "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"
#Загружаем изначальный датафрейм из csv файла при помощи библиотеки pandas
df = pd.read_csv(filename, delimiter=';',header=0, encoding='utf-8', )
#преобразуем df в строку стандартного вида для дальнейшей отправки в CHAT GPT

Part1='\nВ моей таблице есть 4 столбца со следующими названиями:' #первая часть текста для отправки запроса
Part2=''
#собираем вторую часть текста для отправки запроса из названий колонок в таблице
df_colum_name_list=df.columns.values.tolist ()
for colum_name in df_colum_name_list:
    if colum_name!=df_colum_name_list[-1]:
        Part2 = Part2+colum_name+' ,'
    else:
        Part2 = Part2 + colum_name + '.'
Part3=f'\nВ столбце {df.columns[0]} находятся строки со следующими данными:' #третья часть текста для отправки запроса
Part4=''
#Собираем четвертую часть текста для отправки запроса из содержимого первого(0) столбца таболицы
for i in range(0,len(df.index)):
    if i != len(df.index)-1:
        Part4=Part4+df.iloc [i][df.columns[0]]+' ;'
    else:
        Part4 = Part4 + df.iloc[i][df.columns[0]]+'.'
Part5='\nВ столбце review text находятся следующие данные:'
Part6=''#Собираем шестую часть текста для отправки запроса из содержимого второго(1) столбца таболицы
for i in range(0,len(df.index)):
    if i != len(df.index)-1:
        Part6=Part6+df.iloc [i][df.columns[1]]+' ;'
    else:
        Part6 = Part6 + df.iloc[i][df.columns[1]]+'.'
Part7='\nВ столбце date находятся следующие данные:'
Part8=''#Собираем восьмую часть текста для отправки запроса из содержимого третьего(2) столбца таболицы
for i in range(0,len(df.index)):
    if i != len(df.index)-1:
        Part8=Part8+df.iloc [i][df.columns[2]]+' ;'
    else:
        Part8 = Part8 + df.iloc[i][df.columns[2]]+'.'
Part8=Part8+'\nСтолбец rate пустой.'
Part9='Добавь в таблице в столбце rate соответствующее значение целого индекса положительности текста от 1 до 10 для каждого значения в столбце review text. \nУдали из полученной таблицы столбцы: review text , date. \nВыведи результат только в виде текстовой таблицы со столбцами email, rate так чтобы сверху были самые положительные отзывы. В ответе не должно быть ничего кроме текстовой таблицы в формате email,rate.'
content=Part1+Part2+Part3+Part4+Part5+Part6+Part7+Part8+Part9 #собираем текст запроса в одну строку
numbers=[] #определяем переменную numbers, чтобы цикл WHILE не ругался
#Запрашиваем ответ у CHATGPT до тех пор, пока не получим нужный формат ответа (т.к. один и тот же запрос систематически получает разный формат ответа)
while len(df.index)!= len(numbers):
    #Отправляем текст запроса в CHATGPT и получаем ответ
    response = openai.ChatCompletion.create(model="gpt-3.5-turbo",messages=[{"role": "user", "content": content}])
    result = ''
    for choice in response.choices:
        result += choice.message.content
    #Разделяем полученный ответ от CHAT GPT на список Email и список оценок (numbers)
    emails_and_numbers = re.findall(r'([\w\.-]+@[\w\.-]+),(\d+)', result)
    emails = [email for email, number in emails_and_numbers]
    numbers = [number for email, number in emails_and_numbers]
#Выводим на экран текст запроса в CHATGPT
print('\n\n\nТекст отправленного запроса в CHATGPT:', content)
#Выводим на экран текст ответа от CHATGPT
print('\n\n\nТекст полученного ответа CHATGPT:', result)
#создаем новый датафрейм и сохраняем в него значения из списков выше
data = {'email': emails, 'rate': numbers}
df2 = pd.DataFrame(data) #создаем датафрейм
#Создаем имя для нового файла CSV
base, ext = os.path.splitext(filename)
new_filename = base + "_analyzed"
df2.to_csv(f'{new_filename}.csv', index=False, sep=';') # Сохраняем DataFrame2 в файл CSV
print(f'\nПрограмма завершила работу. Файл {new_filename}.csv сохранен в той же директории.')

